## Documentation of Business Process

Task : In Progress
Product Manager: Izul Fathurrahman
Engineers : Aziz Zaeny

### Problem Statement

Create Documentation of Business Process Of Madeinindonesia.

### Proposed work

The Content of Documentation will be created from markdown files. The Engineer will Provide the `.mardkwon` or `.md` files in the structured folders `docs/[language]/[subject]`, assets and image will be placed in the `img/` folders.

Example:

```
docs/
   en/
      Registration-as-buyers.md
Img/
```

Content:

```
## Title
### Overview

![wholeprocess-image](image)

### The Process
```


### Success Criteria

The criteria that must be met in order to consider this project a success.

- It should covers, seller and buyers registrations process.
- It should have product uploading process for vendor.
- It should explain how ordering process happen in madeinindonesia.
- It should explain how the shipment and logistic flow in madeinindonesia.
- It should explain how the payment method and money flow in madeinindonesia.


### Scope
#### Requirements
#### Future Works
#### Non-Requirements

### Design
### Alternatives Considered
### Related Documents
