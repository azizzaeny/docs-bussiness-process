# 1. Uploading Product to Madeinindonesia

![](../../img/uploading-product.png)

### General Overview

- **Product Categories** - Categories—the categories that the product will be assigned to. When a product has multiple categories, the first category on the list will be the main category for the product. To change the main category, just drag another category in its place.
- **Product Name** - the name of the product as it appears on the storefront and in the Administration panel. The name of the same product can differ, depending on the storefront and language.
- **Product Label** - Ready Stock Label and Express Dispatch Label.
- **Product Prices** - base product price in your store’s primary currency. Prices in other currencies are converted automatically, according to the exchange rate that you specify. All calculations are done in the primary currency.
- **Full description** — the product description that will appear on the product details page of the storefront. The description can be either a plain text or a formatted HTML text. If you’re not familiar with HTML code, you can rely on the visual editor: you simply edit product description like a document, and the visual editor adds the HTML code automatically.
- **Status** — the status of the product:
  - **Active** — the product is available on the storefront and appears in the product list.
  - **Disabled** — the product is not available on the storefront.
  - **Hidden** — product does not appear in the product list on the storefront. Customers can reach a hidden product via a direct link.
- **Store** — the storefront to which the product is assigned.
- **Images** — the images of the product. Supported formats are JPEG, GIF, and PNG. The maximum size of an uploaded image depends on your server configuration. As a rule, it should not exceed 2 MB. Thumbnails for these images will be generated automatically.
- **Options Type** - Options are specific properties of the product that a customer can choose when making a purchase. For example, options for clothes may include color and size. The variants of those options would be red/green/blue and S/M/L/XL/XXL respectively.
  - **Simultaneous** — customers can choose the variant for each option independently and in any order.
  - **Sequential** — options and variants are chosen one after another: first the variant of the first option, then the variant of the second option, and so on.

- **Exceptions type** — select a type of the product option exceptions:
   - **Forbidden** —you’ll be able to specify Forbidden combinations on the Options tab. Customers won’t be able to purchase a product when they choose a forbidden combination of option variants.
   - **Allowed** — you’ll be able to specify Allowed combinations on the Options tab. Customers will only be able to select these combinations of option variants.
- **CODE** or **SKU Code**  — the identifier of the product that you use in your store (a stock keeping unit or SKU); it is used to distinguish products from each other during import, so that the data would be saved to the right product.
- **List price** — if this price is higher than product’s Price, then a discount label will be displayed for the product.
- **In stock** — the number of products in the stock. Products that are out of stock can’t be bought, unless you enable the Allow negative amount in inventory setting under Settings → General → Catalog.
- **Zero price action** —determines what can be done on the storefront when the product’s price is 0:
   - Do not allow customers to add the product to cart
   - Allow customers to add the product to cart
   - Ask customers to enter the price — customers will see an input field and will be able to enter the price that they’re willing to pay. That’s useful for charity events or donations.
- **Inventory** — If a product is tracked with options, you’ll need to specify the number of in-stock items individually for each combination of option variants. The way of tracking the inventory of the current product:
  - Track with options
  - Track without options
  - Do not track
- **Minimum order quantity** — the minimum number of items that a customer can buy at a time. This number will appear on the product page on the storefront, right after the product price.
- **Maximum order quantity** — the maximum number of items that a customer can buy at a time. 0 means no limit.
- **Quantity step** — determines the step by which a customer can increase or decrease the number of products in cart.
If the minimum order quantity is 3, the maximum order quantity is 9, and the quantity step is 3, then a customer will be able to purchase 3, 6, or 9 items in one order.
- **List quantity count** — the maximum number of choices in the Quantity drop-down list. It comes useful when you set a quantity step and when there are a lot of items in stock.
- **Taxes** — select the taxes that will be applied to the product.
- **User groups** — the user groups that can access the product’s page.
- **Creation date** — the date when the product was added to the catalog. It is used for sorting products by date.
- **Avail since** — the date when the product becomes available for purchase. It is useful when you want to start selling at a specific date, or when the product isn’t available yet.

- **Out-of-stock actions** — select if customers should be able to buy the product in advance before it is not yet available for sale, or sign up to receive an email notification when the product is available.
   - None — forbid customers from buying the product in one of the following cases:
     When the product is out of stock.
     If you have specified the date when the product would become available (the Avail since property of a product).
   - Buy in advance — make the product available on backorder; customers will be able to order this product, regardless of the availability date and whether or not the product is in stock.
   - Sign up for notification — offer customers to sign up for an email notification so that they receive an email once the product is in stock again.
- **Product details view** — choose a template (basically, the look) of the product page. By default, CS-Cart has 2 templates, but you can develop your own templates according to your needs.
- **Downloadable** — if this checkbox is ticked, the product is labeled as downloadable, i.e. distributed by download.
- **Enable shipping for downloadable products** — if this checkbox is ticked, shipping costs will be calculated for this downloadable product just like for normal products.
- **Time-unlimited download** — if this checkbox is ticked, the product download period never expires.
- **Short description** — a short product description; it appears on the product list on the storefront. If you leave this field blank, the short description will be automatically taken from the full product description (first 300 characters).
- **Popularity** — integer conveying the product popularity, which is calculated automatically. This rating depends on the number of product views as well as the number of times the product was added to the cart and purchased. Products can be sorted by popularity on the storefront.
- **Search words** — the list of words by which the product should be easily found by the built-in search.
- **Promo text** — an additional block with information that appears on the top right side of the product page.

![](../../img/uploading-product-1.png)

### SEO Tabs
- **SEO name** — the value to which the standard URL will be changed.
- **Page title** — the content of the <title></title> container: the title of the product page on the storefront, which is displayed in the web browser when somebody is viewing the page. If you don’t specify a value manually, it will be generated automatically.
- **META description** — the content of the description meta-tag: a brief description of the product.
- **META keywords** — the content of the keywords meta-tag: a list of search keywords that appear on the product page.

![](../../img/uploading-product-2.png)


### Options Tabs

This tab allows you to manage product options and option variants, as well as control the option combinations and add forbidden/allowed combinations.


Product options appear on the product details page on the storefront. Depending on the type of an option, customers either select one of the option variants or provide their own variant. A separate article describes the properties of options.



### Shipping Properties

This tab contains a number of product properties that are important for automatic shipping cost calculation.

- **Weight** — the weight of a single item in the store’s default weight measurement unit. The default weight measurement unit can be specified under Settings → General.
- **Free shipping** — if you tick this checkbox, the product will be excluded from the shipping cost calculation, as long as the shipping method has the Use for free shipping checkbox ticked.
- **Shipping freight** — the handling fee (insurance, packaging, etc.) added to the shipping cost.
- **Items in a box** — the minimum and maximum number of product items to be shipped in a separate box. Usually it’s 1 - 1 (only one product per box).
   - **Box length** — the length of a separate box.
   - **Box width** — the width of a separate box.
   - **Box height** — the height of a separate box.

![](../../img/uploading-product-3.png)


### Quantity Discount Or Wholesale Prices

This tab contains the list of wholesale prices for the product. Customers will see those discounts on the product page on the storefront. Prices apply depending on the number of items of this product in cart.

- **Quantity** — the minimum number of product items to qualify for the product wholesale price.

- **Value** — the value of the discount (per item).

- **Type** — the type of the discount:

- **Absolute** — the cost of 1 discounted item.
- **Percent** — the percent discount off the base product item price.
- **User group** — the user group which can take advantage of the wholesale price. If you set up a discount that applies for all user groups for purchasing 1 item, this will overwrite product price.

![](../../img/uploading-product-4.png)


### Upload Product Process.

1. Go to Vendor Administration Panel by clicking admin in **My Account** Menu.
2. if you are not yet logged in, input credentials **email** and **password** in the login forms.
3. Go to **Product** -> **Product** in the sidebar menu.
4. Click the **Add** Button in top right with plus icon.

![](../../img/uploading-product-5.png)

5. Fill up the required informations marked by * sign.

![](../../img/uploading-product-6.png)


6. Click **Create** or **Save** button on the top right to create or save information of the product





# 2.  Bulk Uploading Product Or Importing Product from CSV/ XML


# 3. View Approved / Disapprove Vendor Products.
