# Registration as Buyers

![registration buyers](../../img/registration-buyers.png)

### General Overview

- **Firstname** - First name of the persons.
- **Lastname** - Last name of the persons
- **Email** - Email Addressed. Email address need to be valid for further registration process.
- **Password** - enter a passwords for the account.
- **Confirm Password** - Re-enter the last password to make sure all the passwords are correct.


### Step By Step Register as Buyer Madeinindonesia

The process of buyer registrations.

![img/reg3.png](../../img/registration-buyers-1.png)

1. Go To **My Account** Menu -> **Registers**
2. Fill up **form registrations** with correct information.
3. **Send data** by clicking submit button.

![img/reg3.png](../../img/registration-buyers-2.png)

4. Check new Email with **new activation links**, folow the links by click on the links.
5. **New Buyers** will active after the activation links clicked
