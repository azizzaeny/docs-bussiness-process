# 1. Registration as a Seller or Vendor

![registration sellers](../../img/registration-sellers.png)

### General Overview

- **Vendor Plan** - Select vendor plan to become a vendors, there are 4 vendor plan Free, Gold, Platinum and Diamon. more details please refer to madeinindonesia vendor plans.
- **Company Name** - Register Name of the company
- **First name** - First name of the persons .
- **last name** - Last name of the persons.
- **E-mail** -  Email Addressed. an active email address for further registration process.

### Vendors Status

- **New** - new registered vendors. vendors does not active yet, the system will automaticly set to the pending after madeinindonesia sent new profile information to the vendors email.
- **Pending** - The application for getting vendors was was approved buy need to review. the vendors need to complete their profiles information.
- **Active** - Active vendors, vendors had been verified and able to sell products in madeinindonesia.

### Step by Step to register as Sellers Madeinindonesia

the process of seller registrations.

1. Follow the steps for registering as buyers of madeinindonesia (click here).
2. Go to **My Account** Menu -> **Admin** or **Become of Sellers**
3. You will be prompted to select vendor plan of madeinindonesia,  and then click **choose vendor plan**.

![](../../img/registration-sellers-1.png)

4. Fill the form registration field with correct informations
5. Click **Submit** for sending registration data.
6. Check for new Email Address to complete registration process by clicking Complete registration form buttons

![](../../img/registration-sellers-2.png)

7. You will redirected for Updating Company Profiles with correct informations, madeinindonesia admin will review the information and approve vendor applications.


# 2. Updating Company Profiles

![](../../img/registration-sellers-update.png)


#### Overview Basic Company Informations

- **Business Categories** - Specify the business categories of the company, it will help vendor for narrowing scope of incoming quotation from the customers or buyers.
- **Street** - Registered Company Address Street.
- **City** - Registered Company Address City.
- **State/Province** - Registered Company Address Province.
- **Zip/Postal Code** - Company Postal Code.
- **Phone** - Company Phones
- **Fax** - Company Fax Addresses.
- **Company Web URL** - optional company hosted web url.
- **Business Type** - Select up to three vendor business types. Manufacturer, Trading Company, Buying Office, Agent, Distributor/ Wholesaler, Goverment Ministry/Bureau/Commission, Asscociation, Business Servicess such as transportation, finances, travel, ads etc
- **Company Logo** - Logo or Image of companies, displayed in the product pages.
- **Detailed Company Introduction** -  Descriptions of company informations
- **Company Photo** - Upload up to three company Photos.
- **Trade Show** - Select if you have attended or planned to attend tradeshows
- **Main Products** and **Others Products You Sell** - Describe Company Main Products.
- **Company Registered Year** - Registered Company Years
- **Total No. Employes** - Describe how many employees company have
- **Legal Owner** - Company Legal Owner Name
- **Office Size** - Office Size in square meters
- **Bank Account**  - Add Bank Account Details
- **Logos** - Company Logos
- **Plan** - Company Vendor Plans.
- **Terms & Conditions** - Terms & Conditions Text Displayed before the checkout pages

Update Company Information Forms.

![](../../img/registration-sellers-update-1.png)

#### Manufacturing Capability
- **Cooperate with Factory** -  Fill the information if you cooperate with factory.
- **Production Process**  - Explain to your customers your production process by uploading several pictures
- **Production Equipment** - Describe your Equipment model and quantity
- **Production Line** -  Display your productions line, such as supervisor number, number of operators, QC/QA number.
- **Factory Location** - Fill information of your factory addresses.
- **Factory Size** - Describe the factory sizes in square meters
- **OEM Service Offered** - Original Equimpment Manufacturer information
- **Design Service Offered** - Check if you have design services that your company offer.
- **OEM Experience** - Provide information about OEM experinces in years
- **No. QC Staff** - Provide information about quality assurances staff.
- **No. R&D Staff** - Provide Information about R&D Staff.
- **Annual Output Value** - Provide information about your annual output values in USD.
- **Annual Production Capacity** - Provide information in details about your annual production capacity.

### Quality Control
- **Quality Control Process** - Upload images Information Of QC Process.
- **Testing Equipment** - Display Testing Equipment information.

#### Research and Development
- **Display R&D Process** - Describe your company R&D Process By uploading several pictures.

#### Export Capabilities
- **Export Percentage** - Provide information of the Export Percentage.
- **Total Annual revenue** - Provide information of your company annual revenue
- **Main Market Distribution** - Describe your markets distributions.
- **Year company exporting** - Provide information when your company started exporting.
- **Add Customer Case** - Display information of your exporting project by the custumer cases.
- **No. Employees in Trade Departement** - Provide information of no. employees in your trade departement.
- **Nearest Port** - Nearest Port that you had been Exporting
- **Average Lead Time** - Display information of how long your average lead time.
- **Overseas Offices** - Fill up the information if you have overseas offices.
- **Accepted Delivery Terms** - Check your accepted trade terms.
- **Accepted Payment Currencies** - Check your acepted payment currencies.
- **Accepted Payment Type** - Check your acepted payment type.
- **Language Spooken** - Describe your main communication languages.

#### Certificate Center
- **Display Certificate** - Upload your certification informations
- **Display Honors and Awards**  - Upload your award or honor informations
- **Display Pattents** - Provide information about your pattent numbers
- **Display Trademarks** - Provide information about your company trademarks.

# 3. Become a Verified Seller (Activation and Approval)

![registration sellers](../../img/registration-sellers-verified.png)

1. After you submit **Update Company Information Profile**, the status of the vendors will be **pending** waiting for approval.

2. Vendors need to provide **NIB/SIUP/TDP** Documents to the madeinindonesia. Madeinindonesia admin will check and review it before activating your company as seller of madeinindonesia,

3. Admin madeinindonesia will **Contacts Seller** for futher informations. If vendors applicants approved/decline, mind system will send acceptance and seller guide via emails.

4. Upload your details **KYC documents** to become a verified sellers of madeinindonesia

5. Vendors can upload and sells their products.
